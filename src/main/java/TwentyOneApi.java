import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
@ComponentScan("twentyOne")
public class TwentyOneApi {
    public static void main(String[] args) {
        SpringApplication.run(TwentyOneApi.class, args);
    }
}
