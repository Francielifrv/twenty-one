package twentyOne.player;

import twentyOne.deck.Card;
import twentyOne.models.player.PlayerStatus;

import java.util.Set;

public class Player {
    private String name;
    private Set<Card> hand;
    private int score;
    private Long bet;
    private PlayerStatus status;

    public Player() {}

    public Player(String name, Set<Card> cards) {
        this.name = name;
        this.hand = cards;

        this.score = calculateScore(cards);
        this.status = PlayerStatus.READY_TO_BET;
    }

    private int calculateScore(Set<Card> cards) {
        return cards.stream().mapToInt(card -> card.getRank().getValue()).sum();
    }

    public String getName() {
        return name;
    }

    public Set<Card> getHand() {
        return hand;
    }

    public int getScore() {
        return score;
    }

    public Long getBet() {
        return bet;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public boolean isReadyToPlay(){
        return status.equals(PlayerStatus.READY_TO_PLAY);
    }

    public void setBet(Long bet) {
        this.bet = bet;
    }

    public boolean canPlay() {
        return this.status.equals(PlayerStatus.PLAYING);
    }

    private void addCard(Card card) {
        this.hand.add(card);
        this.score += card.getRank().getValue();
    }

    public void hit(Card card) {
        addCard(card);

        if (this.score > 21) {
            this.status = PlayerStatus.LOST;
        }
    }
}
