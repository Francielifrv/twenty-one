package twentyOne.player;

import org.springframework.stereotype.Component;
import twentyOne.deck.Deck;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerGenerator {
    public List<Player> generatePlayers (Deck deck, List<String> playerNames) {
        return playerNames
                .stream()
                .map(name -> new Player(name, deck.initialHit())).collect(Collectors.toList());
    }
}
