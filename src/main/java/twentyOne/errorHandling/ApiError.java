package twentyOne.errorHandling;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiError {
    private String errorMessage;

    public ApiError(@JsonProperty("errorMessage") String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonProperty("errorMessage")
    public String getErrorMessage() {
        return errorMessage;
    }
}
