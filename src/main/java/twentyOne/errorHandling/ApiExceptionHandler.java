package twentyOne.errorHandling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import twentyOne.exceptions.InvalidPlayerMoveException;
import twentyOne.exceptions.NotFoundException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity handleCarChargingSessionNotFoundException(NotFoundException exception) {
        ApiError error = new ApiError(exception.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(value = InvalidPlayerMoveException.class)
    public ResponseEntity<ApiError> handleInvalidPlayerMoveException(InvalidPlayerMoveException exception) {
        ApiError error = new ApiError(exception.getMessage());

        return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
    }
}
