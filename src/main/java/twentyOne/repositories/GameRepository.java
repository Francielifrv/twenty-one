package twentyOne.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import twentyOne.game.Game;

public interface GameRepository extends MongoRepository<Game, String> {
}
