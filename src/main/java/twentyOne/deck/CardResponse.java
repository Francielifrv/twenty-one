package twentyOne.deck;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardResponse {
    @JsonProperty("rank")
    private final Rank rank;

    @JsonProperty("suite")
    private final Suit suit;

    public CardResponse(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public static CardResponse fromCard(Card card) {
        return new CardResponse(card.getRank(), card.getSuit());
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }
}
