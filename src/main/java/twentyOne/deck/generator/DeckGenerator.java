package twentyOne.deck.generator;

import org.springframework.stereotype.Component;
import twentyOne.deck.Card;
import twentyOne.deck.Deck;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;

import java.util.*;

@Component
public class DeckGenerator {

    public Deck generateShuffledDeck() {
        List<Card> cards = generateCards();
        Collections.shuffle(cards);

        return new Deck(new LinkedList<>(cards));
    }

    private List<Card> generateCards() {
        Rank[] ranks = Rank.values();
        Suit[] suits = Suit.values();

        List<Card> cards = new ArrayList<>();

        for (Rank rank : ranks) {
            for (Suit suit : suits) {
                cards.add(new Card(rank, suit));
            }
        }

        return cards;
    }
}
