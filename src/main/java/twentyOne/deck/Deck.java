package twentyOne.deck;

import java.util.*;

public class Deck {
    private LinkedList<Card> cards;

    public Deck() {
    }

    public Deck(LinkedList<Card> cards) {
        this.cards = cards;
    }

    public Queue<Card> getCards() {
        return cards;
    }

    public Card hit() {
        return cards.poll();
    }

    public Set<Card> initialHit() {
        return new HashSet<>(Arrays.asList(cards.poll(), cards.poll()));
    }
}
