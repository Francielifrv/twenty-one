package twentyOne.deck;

public enum Suit {
    SPADE, HEART, DIAMOND, CLUB
}