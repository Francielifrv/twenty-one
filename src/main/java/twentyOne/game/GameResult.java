package twentyOne.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import twentyOne.deck.Card;

import java.util.List;
import java.util.Set;

public class GameResult {
    @JsonProperty("dealerHand")
    private final Set<Card> dealerHand;
    private final String winner;
    private final List<PlayerScoreResponse> playersScore;

    public GameResult(Set<Card> dealerHand, String winner, List<PlayerScoreResponse> playersScore) {
        this.dealerHand = dealerHand;
        this.winner = winner;
        this.playersScore = playersScore;
    }
}
