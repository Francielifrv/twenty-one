package twentyOne.game.action;

import twentyOne.game.Game;
import twentyOne.player.Player;

public class ActionFactory {
    public static GameActionStrategy getStrategy(GameAction action, Game game, Player player) {
        return action.equals(GameAction.HIT) ? new HitActionStrategy(game, player) : new StandActionStrategy(game, player);
    }
}
