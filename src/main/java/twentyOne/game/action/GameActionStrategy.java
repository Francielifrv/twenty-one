package twentyOne.game.action;

public interface GameActionStrategy {
    void play();
}
