package twentyOne.game.action;

import twentyOne.game.Game;
import twentyOne.player.Player;

public class HitActionStrategy implements GameActionStrategy {

    private Game game;
    private Player player;

    public HitActionStrategy(Game game, Player player) {
        this.game = game;
        this.player = player;
    }

    @Override
    public void play() {
        game.hit(player);
    }
}
