package twentyOne.game.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twentyOne.deck.Deck;
import twentyOne.deck.generator.DeckGenerator;
import twentyOne.game.Game;
import twentyOne.models.game.GameStatus;
import twentyOne.player.Player;
import twentyOne.player.PlayerGenerator;

import java.util.List;

@Component
public class GameGenerator {

    private PlayerGenerator playerGenerator;
    private DeckGenerator deckGenerator;

    @Autowired
    public GameGenerator(PlayerGenerator playerGenerator, DeckGenerator deckGenerator) {
        this.playerGenerator = playerGenerator;
        this.deckGenerator = deckGenerator;
    }

    public Game generate(List<String> playerNames) {

        Deck deck = deckGenerator.generateShuffledDeck();

        Player dealer = new Player("Dealer", deck.initialHit());
        List<Player> players = playerGenerator.generatePlayers(deck, playerNames);

        return Game.builder()
                .dealer(dealer)
                .players(players)
                .status(GameStatus.STARTED)
                .deck(deck).build();
    }
}
