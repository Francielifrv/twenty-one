package twentyOne.game;

import lombok.Builder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import twentyOne.deck.Deck;
import twentyOne.exceptions.InvalidPlayerMoveException;
import twentyOne.exceptions.NotFoundException;
import twentyOne.models.game.GameStatus;
import twentyOne.models.player.PlayerStatus;
import twentyOne.player.Player;

import java.util.List;

@Document
@Builder
public class Game {
    @Id
    private String id;
    private Player dealer;
    private List<Player> players;
    private GameStatus status;

    private Deck deck;

    public void setId(String id) {
        this.id = id;
    }

    public void setDealer(Player dealer) {
        this.dealer = dealer;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public Player getDealer() {
        return dealer;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public GameStatus getStatus() {
        return status;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public Player getPlayerByName(String name) {
        return players.stream()
                .filter(player -> player.getName().equals(name))
                .findFirst()
                .orElseThrow(() ->
                        new NotFoundException("Could not find player with name " + name + " for game with id " + this.id));
    }

    public boolean haveAllPlayersBet() {
        return players.stream()
                .filter(Player::isReadyToPlay)
                .count() == players.size();
    }

    public void setFirstPlayer() {
        if (players.size() > 0) {
            this.players.get(0).setStatus(PlayerStatus.PLAYING);
        }
    }

    public void hit(Player player) {
        if (player.canPlay()) {
            player.hit(deck.hit());
        } else {
            throw new InvalidPlayerMoveException(String.format("Cannot perform hit operation, player <%s> is not " +
                    "currently playing.", player.getName()));
        }
    }

    public void stand(Player player) {
        if (player.canPlay()) {
            player.setStatus(PlayerStatus.STAND);
        } else {
            throw new InvalidPlayerMoveException(String.format("Cannot perform hit operation, player <%s> is not " +
                    "currently playing.", player.getName()));
        }
    }
}

