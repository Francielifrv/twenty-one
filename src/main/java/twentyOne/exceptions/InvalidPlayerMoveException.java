package twentyOne.exceptions;

public class InvalidPlayerMoveException extends RuntimeException {
    public InvalidPlayerMoveException(String message) {
        super(message);
    }
}
