package twentyOne.controlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import twentyOne.game.action.GameAction;
import twentyOne.models.game.CreateGameRequest;
import twentyOne.models.game.GameResponse;
import twentyOne.services.GameService;

@RestController
@RequestMapping("/twenty-one")
public class TwentyOneController {
    private GameService service;

    @Autowired
    public TwentyOneController(GameService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseBody
    public GameResponse startNewGame(@RequestBody CreateGameRequest request) {
        return service.createNewGame(request);
    }


    @PutMapping("/{gameId}/player/{playerName}/bet")
    public GameResponse placeBet(@PathVariable("gameId") String gameId,
                                 @PathVariable("playerName") String playerName,
                                 @RequestParam("amount") Long amount) {
        return service.placeBet(gameId, playerName, amount);
    }

    @PutMapping("/{gameId}/player/{playerName}")
    public GameResponse play(@PathVariable("gameId") String gameId,
                             @PathVariable("playerName") String playerName,
                             @RequestParam("action") GameAction action) {

        return service.play(gameId, playerName, action);
    }
}
