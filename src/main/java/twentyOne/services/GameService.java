package twentyOne.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twentyOne.exceptions.NotFoundException;
import twentyOne.game.Game;
import twentyOne.game.action.ActionFactory;
import twentyOne.game.action.GameAction;
import twentyOne.game.action.GameActionStrategy;
import twentyOne.game.generator.GameGenerator;
import twentyOne.models.game.CreateGameRequest;
import twentyOne.models.game.GameResponse;
import twentyOne.models.game.GameStatus;
import twentyOne.models.player.PlayerStatus;
import twentyOne.player.Player;
import twentyOne.repositories.GameRepository;

@Service
public class GameService {
    private GameGenerator gameGenerator;
    private GameRepository gameRepository;

    private final String DEALER = "Dealer";

    @Autowired
    public GameService(GameGenerator gameGenerator, GameRepository gameRepository) {
        this.gameGenerator = gameGenerator;
        this.gameRepository = gameRepository;
    }

    public GameResponse createNewGame(CreateGameRequest request) {
        Game game = gameGenerator.generate(request.getPlayerNames());

        Game persistedGame = gameRepository.save(game);

        return GameResponse.fromGame(persistedGame);
    }

    public GameResponse placeBet(String gameId, String playerName, Long amount) {
        Game game = fetchGame(gameId);

        Player player = game.getPlayerByName(playerName);
        player.setBet(amount);
        player.setStatus(PlayerStatus.READY_TO_PLAY);

        if (game.haveAllPlayersBet()) {
            game.setStatus(GameStatus.PLAYERS_PLAYING);
            game.setFirstPlayer();
        }

        gameRepository.save(game);

        return GameResponse.fromGame(game);
    }

    private Game fetchGame(String gameId) {
        return gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException("Could not find game with id " + gameId));
    }

    public GameResponse play(String gameId, String playerName, GameAction action) {
        Game game = fetchGame(gameId);


        Player player = getPlayer(game, playerName);

        GameActionStrategy strategy = ActionFactory.getStrategy(action, game, player);
        strategy.play();

        return GameResponse.fromGame(gameRepository.save(game));
    }

    private Player getPlayer(Game game, String playerName) {
        return playerName.equals(DEALER) ? game.getDealer() : game.getPlayerByName(playerName);
    }
}
