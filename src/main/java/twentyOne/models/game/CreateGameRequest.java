package twentyOne.models.game;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;
import java.util.List;

public class CreateGameRequest {
    @Size(min = 1, max = 3, message = "There must be 1 to 3 players on table to start a new game round")
    private final List<String> playerNames;

    public CreateGameRequest(@JsonProperty("player_names") List<String> playerNames) {
        this.playerNames = playerNames;
    }

    @JsonProperty("player_names")
    public List<String> getPlayerNames() {
        return playerNames;
    }
}
