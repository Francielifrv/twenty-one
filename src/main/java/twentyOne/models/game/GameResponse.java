package twentyOne.models.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import twentyOne.game.Game;
import twentyOne.models.player.PlayerResponse;

import java.util.List;
import java.util.stream.Collectors;

public class GameResponse {

    @JsonProperty("id")
    private final String id;

    @JsonProperty("status")
    private final GameStatus status;

    @JsonProperty("dealer")
    private final PlayerResponse dealer;

    @JsonProperty("players")
    private final List<PlayerResponse> players;

    public GameResponse(String id, GameStatus status, PlayerResponse dealer, List<PlayerResponse> players) {
        this.id = id;
        this.status = status;
        this.dealer = dealer;
        this.players = players;
    }

    public static GameResponse fromGame(Game game) {
        PlayerResponse dealer = PlayerResponse.fromPlayer(game.getDealer());
        List<PlayerResponse> players = game.getPlayers()
                .stream()
                .map(PlayerResponse::fromPlayer)
                .collect(Collectors.toList());

        return new GameResponse(game.getId(), game.getStatus(), dealer, players);
    }

    public String getId() {
        return id;
    }

    public GameStatus getStatus() {
        return status;
    }

    public PlayerResponse getDealer() {
        return dealer;
    }

    public List<PlayerResponse> getPlayers() {
        return players;
    }
}
