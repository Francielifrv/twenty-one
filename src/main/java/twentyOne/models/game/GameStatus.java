package twentyOne.models.game;

public enum GameStatus {
    STARTED, PLAYERS_PLAYING
}
