package twentyOne.models.player;

import com.fasterxml.jackson.annotation.JsonProperty;
import twentyOne.deck.CardResponse;
import twentyOne.player.Player;

import java.util.Set;
import java.util.stream.Collectors;

public class PlayerResponse {
    @JsonProperty("cards")
    private final Set<CardResponse> cards;

    @JsonProperty("status")
    private final PlayerStatus status;

    @JsonProperty("name")
    private final String name;

    @JsonProperty("score")
    private final int score;

    @JsonProperty("bet")
    private final Long bet;

    public PlayerResponse(Set<CardResponse> cards, PlayerStatus status, String name, int score, Long bet) {
        this.cards = cards;
        this.status = status;
        this.name = name;
        this.score = score;
        this.bet = bet;
    }

    public static PlayerResponse fromPlayer(Player player) {
        Set<CardResponse> cards = createCardResponse(player);

        return new PlayerResponse(cards, player.getStatus(), player.getName(), player.getScore(), player.getBet());
    }

    private static Set<CardResponse> createCardResponse(Player player) {
        return player.getHand()
                .stream().map(CardResponse::fromCard)
                .collect(Collectors.toSet());
    }

    public Set<CardResponse> getCards() {
        return cards;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }
}
