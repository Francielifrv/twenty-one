package twentyOne.models.player;

public enum PlayerStatus {
    READY_TO_BET, READY_TO_PLAY, PLAYING, STAND, LOST
}
