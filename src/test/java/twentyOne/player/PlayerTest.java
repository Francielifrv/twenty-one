package twentyOne.player;

import org.junit.Before;
import org.junit.Test;
import twentyOne.deck.Card;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.models.player.PlayerStatus;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PlayerTest {
    private Player player;

    @Before
    public void setUp() {
        player = createPlayer();
    }

    @Test
    public void calculateScoreBasedOnCards() {
        int expectedScore = 12;

        assertThat(player.getScore(), is(expectedScore));
    }

    @Test
    public void initializesPlayerWithStatusReadyToBet() {
        assertThat(player.getStatus(), is(PlayerStatus.READY_TO_BET));
    }

    private Player createPlayer() {
        Set<Card> cards = new HashSet<>(Arrays.asList(new Card(Rank.EIGHT, Suit.SPADE), new Card(Rank.FOUR, Suit.CLUB)));

        return new Player("player", cards);
    }
}