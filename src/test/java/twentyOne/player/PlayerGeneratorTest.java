package twentyOne.player;

import org.junit.Test;
import twentyOne.deck.Card;
import twentyOne.deck.Deck;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PlayerGeneratorTest {

    @Test
    public void generatesListOfPlayers() {
        PlayerGenerator generator = new PlayerGenerator();
        Deck deck = getDeck();
        List<String> playerNames = Arrays.asList("aPlayer", "anotherPlayer");

        List<Player> players = generator.generatePlayers(deck, playerNames);

        assertThat(players, hasSize(2));

        assertThat(players.get(0).getName(), is("aPlayer"));
        assertThat(players.get(1).getName(), is("anotherPlayer"));
    }

    private Deck getDeck() {
        LinkedList<Card> cards = new LinkedList<>(Arrays.asList(
                new Card(Rank.EIGHT, Suit.CLUB),
                new Card(Rank.QUEEN, Suit.DIAMOND),
                new Card(Rank.FOUR, Suit.CLUB),
                new Card(Rank.EIGHT, Suit.SPADE)));

        return new Deck(cards);
    }
}