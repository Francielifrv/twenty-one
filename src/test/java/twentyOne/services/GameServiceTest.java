package twentyOne.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import twentyOne.deck.Card;
import twentyOne.deck.Deck;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.exceptions.NotFoundException;
import twentyOne.game.Game;
import twentyOne.game.generator.GameGenerator;
import twentyOne.models.game.CreateGameRequest;
import twentyOne.models.game.GameResponse;
import twentyOne.models.game.GameStatus;
import twentyOne.models.player.PlayerStatus;
import twentyOne.player.Player;
import twentyOne.repositories.GameRepository;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GameServiceTest {

    private static final CreateGameRequest REQUEST = new CreateGameRequest(singletonList("player1"));
    private static final String GAME_ID = "5db12828de30f45b090e54c8";
    private static final String PLAYER_NAME = "player1";
    private static final Long BET = 300L;


    @Mock
    private GameGenerator generator;

    @Mock
    private GameRepository repository;

    private Game game;
    private GameService service;

    @Before
    public void setUp() {
        initMocks(this);
        game = createGame();

        service = new GameService(generator, repository);

        when(generator.generate(REQUEST.getPlayerNames())).thenReturn(game);
        when(repository.save(ArgumentMatchers.any(Game.class))).thenReturn(game);
        when(repository.findById(GAME_ID)).thenReturn(Optional.of(game));
    }

    @Test
    public void shouldCreateShuffledDeck() {
        CreateGameRequest request = REQUEST;

        service.createNewGame(request);

        verify(generator).generate(request.getPlayerNames());
    }

    @Test
    public void persistsNewGame() {
        service.createNewGame(REQUEST);

        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(repository).save(gameArgumentCaptor.capture());

        Game persistedGame = gameArgumentCaptor.getValue();
        assertThat(persistedGame, is(game));
    }

    @Test
    public void returnsCreateGameResponse() {
        GameResponse response = service.createNewGame(REQUEST);

        assertThat(response.getId(), is(nullValue()));
        assertThat(response.getPlayers(), hasSize(1));
        assertThat(response.getStatus(), is(GameStatus.STARTED));
        assertThat(response.getDealer(), is(notNullValue()));
    }

    @Test
    public void findsGameToHaveItsBetPlaced() {
        service.placeBet(GAME_ID, PLAYER_NAME, BET);

        verify(repository).findById(GAME_ID);
    }

    @Test(expected = NotFoundException.class)
    public void throwsNotFoundExceptionWhenGameDoesNotExist() {
        when(repository.findById(GAME_ID)).thenReturn(Optional.empty());

        service.placeBet(GAME_ID, PLAYER_NAME, BET);
    }


    @Test
    public void updatesGameOnPlaceBet() {
        service.placeBet(GAME_ID, PLAYER_NAME, BET);

        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(repository).save(gameArgumentCaptor.capture());

        Game persistedGame = gameArgumentCaptor.getValue();

        Player player = persistedGame.getPlayers().get(0);
        assertThat(player.getBet(), is(BET));
        assertThat(player.getStatus(), is(PlayerStatus.PLAYING));
    }

    private Game createGame() {
        Deck deck = getDeck();

        List<Player> players = singletonList(new Player(PLAYER_NAME, deck.initialHit()));
        Player dealer = new Player("Dealer", deck.initialHit());

        return Game.builder()
                .dealer(dealer)
                .players(players)
                .status(GameStatus.STARTED)
                .deck(deck).build();
    }

    private Deck getDeck() {
        LinkedList<Card> cards = new LinkedList<>(Arrays.asList(
                new Card(Rank.EIGHT, Suit.CLUB),
                new Card(Rank.QUEEN, Suit.DIAMOND),
                new Card(Rank.FOUR, Suit.CLUB),
                new Card(Rank.EIGHT, Suit.SPADE)));

        return new Deck(cards);
    }
}