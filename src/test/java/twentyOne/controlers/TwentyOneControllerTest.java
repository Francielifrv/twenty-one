package twentyOne.controlers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import twentyOne.deck.CardResponse;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.game.Game;
import twentyOne.models.game.CreateGameRequest;
import twentyOne.models.game.GameResponse;
import twentyOne.models.game.GameStatus;
import twentyOne.models.player.PlayerResponse;
import twentyOne.models.player.PlayerStatus;
import twentyOne.services.GameService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TwentyOneControllerTest {

    private static final String PLAYER_NAME = "player1";
    private static final String GAME_ID = "id";
    private static final Long AMOUNT = 50L;

    @Mock
    private GameService service;

    private Game game;
    private CreateGameRequest request;
    private GameResponse response;
    private TwentyOneController controller;

    @Before
    public void setUp() {
        initMocks(this);
        request = new CreateGameRequest(singletonList(PLAYER_NAME));
        response = buildCreateGameResponse();
        controller = new TwentyOneController(service);
    }

    @Test
    public void shouldStartNewGame() {
        controller.startNewGame(request);

        verify(service).createNewGame(request);
    }

    @Test
    public void returnsCreateGameResponse() {
        when(service.createNewGame(request)).thenReturn(response);

        GameResponse gameResponse = controller.startNewGame(request);

        assertThat(gameResponse, is(response));
    }

    @Test
    public void shouldPlaceBet() {
        when(service.placeBet(GAME_ID, PLAYER_NAME, AMOUNT)).thenReturn(response);

        GameResponse gameResponse = controller.placeBet(GAME_ID, PLAYER_NAME, AMOUNT);
        verify(service).placeBet(GAME_ID, PLAYER_NAME, AMOUNT);

        assertThat(gameResponse, is(response));
    }

    private GameResponse buildCreateGameResponse() {
        CardResponse card = new CardResponse(Rank.ACE, Suit.CLUB);
        CardResponse anotherCard = new CardResponse(Rank.EIGHT, Suit.HEART);
        Set<CardResponse> cards = new HashSet<>(Arrays.asList(card, anotherCard));

        PlayerResponse dealer = new PlayerResponse(cards, PlayerStatus.READY_TO_BET, "dealer", 0, 100L);
        PlayerResponse player = new PlayerResponse(cards, PlayerStatus.READY_TO_BET, "player1", 0, 300L);

        return new GameResponse("id", GameStatus.STARTED, dealer, singletonList(player));
    }
}