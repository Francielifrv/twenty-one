package twentyOne.game;

import org.junit.Test;
import twentyOne.exceptions.NotFoundException;
import twentyOne.player.Player;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GameTest {
    @Test
    public void findPlayersByName() {
        Player player = new Player("player", Collections.emptySet());
        Player anotherPlayer = new Player("anotherPlayer", Collections.emptySet());

        Game game = Game.builder().players(Arrays.asList(player, anotherPlayer)).build();

        Player foundPlayer = game.getPlayerByName("player");

        assertThat(player, is(foundPlayer));
    }

    @Test(expected = NotFoundException.class)
    public void throwsNotFoundExceptionWhenGameIsNotFound() {
        Game game = Game.builder().players(Collections.emptyList()).build();

        game.getPlayerByName("player");
    }
}