package twentyOne.game.generator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import twentyOne.deck.Card;
import twentyOne.deck.Deck;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.deck.generator.DeckGenerator;
import twentyOne.game.Game;
import twentyOne.models.game.GameStatus;
import twentyOne.player.Player;
import twentyOne.player.PlayerGenerator;

import java.util.*;

import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GameGeneratorTest {
    private static final List<String> PLAYER_NAMES = singletonList("player");

    @Mock
    private DeckGenerator deckGenerator;

    @Mock
    private PlayerGenerator playerGenerator;

    private Deck deck;
    private List<Player> players;
    private GameGenerator gameGenerator;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        gameGenerator = new GameGenerator(playerGenerator, deckGenerator);

        deck = getDeck();
        when(deckGenerator.generateShuffledDeck()).thenReturn(deck);

        players = Collections.singletonList(new Player("player", emptySet()));
        when(playerGenerator.generatePlayers(deck, PLAYER_NAMES)).thenReturn(players);
    }

    @Test
    public void generatesDeckOnGameCreation() {
        gameGenerator.generate(PLAYER_NAMES);

        verify(deckGenerator).generateShuffledDeck();
    }

    @Test
    public void generatesPlayersOnGameCreation() {
        gameGenerator.generate(PLAYER_NAMES);

        verify(playerGenerator).generatePlayers(deck, PLAYER_NAMES);
    }

    @Test
    public void generatesInitialGameConfiguration() {
        Game game = gameGenerator.generate(PLAYER_NAMES);

        assertThat(game.getStatus(), is(GameStatus.STARTED));
        assertThat(game.getDealer().getName(), is("Dealer"));
        assertThat(game.getDeck(), is(deck));
        assertThat(game.getPlayers(), is(players));
    }

    private Deck getDeck() {
        LinkedList<Card> cards = new LinkedList<>(Arrays.asList(
                new Card(Rank.EIGHT, Suit.CLUB),
                new Card(Rank.QUEEN, Suit.DIAMOND),
                new Card(Rank.FOUR, Suit.CLUB),
                new Card(Rank.EIGHT, Suit.SPADE)));

        return new Deck(cards);
    }
}