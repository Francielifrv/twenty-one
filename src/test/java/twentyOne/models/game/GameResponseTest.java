package twentyOne.models.game;

import org.junit.Test;
import twentyOne.deck.Card;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.game.Game;
import twentyOne.models.player.PlayerResponse;
import twentyOne.player.Player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GameResponseTest {
    @Test
    public void createsFromEntity() {
        Game game = createGameEntity();

        GameResponse response = GameResponse.fromGame(game);

        assertThat(response.getId(), is(game.getId()));
        assertThat(response.getPlayers(), hasSize(game.getPlayers().size()));

        PlayerResponse dealer = response.getDealer();
        assertThat(dealer.getName(), is(game.getDealer().getName()));
        assertThat(dealer.getStatus(), is(game.getDealer().getStatus()));
    }

    private Game createGameEntity() {
        Set<Card> cards = createCards();
        List<Player> players = Arrays.asList(new Player("player", cards), new Player("otherPlayer", cards));

        return Game.builder().players(players)
                .dealer(new Player("Dealer", cards))
                .status(GameStatus.STARTED).id("gameId").build();
    }

    private Set<Card> createCards() {
        return new HashSet<>(Arrays.asList(new Card(Rank.EIGHT, Suit.SPADE),
                new Card(Rank.JACK, Suit.CLUB)));
    }
}