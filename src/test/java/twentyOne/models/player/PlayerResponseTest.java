package twentyOne.models.player;

import org.junit.Test;
import twentyOne.deck.Card;
import twentyOne.deck.Rank;
import twentyOne.deck.Suit;
import twentyOne.player.Player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PlayerResponseTest {

    private static final String PLAYER_NAME = "player";

    @Test
    public void createsResponseFromEntity() {
        Player entity = createPlayerEntity();

        PlayerResponse playerResponse = PlayerResponse.fromPlayer(entity);

        assertThat(playerResponse.getName(), is(PLAYER_NAME));
        assertThat(playerResponse.getCards(), hasSize(entity.getHand().size()));
    }

    private Player createPlayerEntity() {
        Card card = new Card(Rank.FOUR, Suit.CLUB);
        Card anotherCard = new Card(Rank.EIGHT, Suit.SPADE);

        Set<Card> cards = new HashSet<>(Arrays.asList(card, anotherCard));

        return new Player(PLAYER_NAME, cards);
    }
}