package twentyOne.deck;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CardResponseTest {
    @Test
    public void createsFromEntity() {
        Card entity = new Card(Rank.JACK, Suit.DIAMOND);

        CardResponse cardResponse = CardResponse.fromCard(entity);

        assertThat(cardResponse.getRank(), is(Rank.JACK));
        assertThat(cardResponse.getSuit(), is(Suit.DIAMOND));
    }
}