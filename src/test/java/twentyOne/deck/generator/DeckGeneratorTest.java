package twentyOne.deck.generator;

import org.junit.Before;
import org.junit.Test;
import twentyOne.deck.Card;
import twentyOne.deck.Deck;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class DeckGeneratorTest {
    DeckGenerator deckGenerator;

    @Before
    public void setUp() {
        deckGenerator = new DeckGenerator();
    }

    @Test
    public void generatesDeckWith52Cards() {
        Deck deck = deckGenerator.generateShuffledDeck();

        assertThat(deck.getCards(), hasSize(52));
    }

    @Test
    public void allCardsOnDeckAreUnique() {
        Deck deck = deckGenerator.generateShuffledDeck();

        Set<Card> uniqueCards = new HashSet<>(deck.getCards());

        assertThat(deck.getCards().size(), is(uniqueCards.size()));
    }

    @Test
    public void deckIsShuffled() {
        Deck deck = deckGenerator.generateShuffledDeck();
        Card deckCard = deck.hit();

        Deck anotherDeck = deckGenerator.generateShuffledDeck();
        Card anotherDeckCard = anotherDeck.hit();

        assertThat(deckCard, is(not(anotherDeckCard)));
    }
}