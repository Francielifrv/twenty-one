package twentyOne.deck;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DeckTest {
    private Deck deck;

    @Before
    public void setUp() {
        LinkedList<Card> cards = new LinkedList<>(Arrays.asList(
                new Card(Rank.EIGHT, Suit.CLUB),
                new Card(Rank.QUEEN, Suit.DIAMOND),
                new Card(Rank.FOUR, Suit.CLUB),
                new Card(Rank.EIGHT, Suit.SPADE)));

        deck = new Deck(cards);
    }

    @Test
    public void hitsACardOnDeck() {
        Card card = deck.hit();

        assertThat(card.getRank(), is(Rank.EIGHT));
        assertThat(card.getSuit(), is(Suit.CLUB));
    }

    @Test
    public void amountOfCardsOnDeckIsDecreasedAfterHit() {
        deck.hit();

        assertThat(deck.getCards(), hasSize(3));
    }

    @Test
    public void retrievesTwoCardsOnInitialHit() {
        Set<Card> cards = deck.initialHit();

        assertThat(cards, hasSize(2));
    }

    @Test
    public void decreasesDeckSizeByTwoAfterInitialHit() {
        deck.initialHit();

        assertThat(deck.getCards(), hasSize(2));
    }
}