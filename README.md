# statistics_api

#### Dependencies
* Java 11
* [Gradle](https://gradle.org/)

#### Running tests

`gradle test` or
`./gradlew test`

#### Building the application

`gradle build` or
`./gradlew build`

#### Running the application

`gradle bootRun` or `./gradlew bootRun`

#### Swagger documentation
API documentation can be found on http://localhost:9001/swagger-ui.html#




